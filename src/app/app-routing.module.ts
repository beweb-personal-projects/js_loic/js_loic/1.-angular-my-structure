import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { InboxComponent } from './core/pages/inbox/inbox.component';
import { SendmailComponent } from './core/pages/sendmail/sendmail.component';
import { SentComponent } from './core/pages/sent/sent.component';

const routes: Routes = [
  {
    path: 'sendmail', component: SendmailComponent
  },
  {
    path: 'inbox', component: InboxComponent
  },
  {
    path: 'sent', component: SentComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './templates/footer/footer.component';
import { NavbarComponent } from './templates/navbar/navbar.component';
import { SidebarComponent } from './templates/sidebar/sidebar.component';
import { PagesComponent } from './pages/pages.component';
import { NbActionsModule, NbCardModule, NbMenuModule, NbSidebarModule } from '@nebular/theme';
import { SendmailComponent } from './pages/sendmail/sendmail.component';
import { InboxComponent } from './pages/inbox/inbox.component';
import { SentComponent } from './pages/sent/sent.component';
import { AppRoutingModule } from '../app-routing.module';
import { BrowserModule } from '@angular/platform-browser';


@NgModule({
  declarations: [
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    PagesComponent,
    SendmailComponent,
    InboxComponent,
    SentComponent,
  ],
  imports: [
    CommonModule,
    NbActionsModule,
    NbSidebarModule,
    NbMenuModule,
    NbCardModule,
    AppRoutingModule,
    BrowserModule
  ],
  exports: [
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    PagesComponent,
  ]
})
export class CoreModule { }

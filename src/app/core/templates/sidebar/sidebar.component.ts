import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { SIDEBAR_LINKS } from './sidebar_links';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  items = SIDEBAR_LINKS;
}

import { NbMenuItem } from "@nebular/theme";

export const SIDEBAR_LINKS: NbMenuItem[] = [
  {
    title: 'Home',
    icon: 'person-outline',
    link: '/home'
  },
  {
    title: 'Mail',
    icon: 'email-outline',
    expanded: true,
    children: [
      {
        title: 'Send Mail',
        link: '/sendmail'
      },
      {
        title: 'Inbox',
        link: '/inbox'
      },
      {
        title: 'Sent',
        link: '/sent'
      },
    ],
  },
];
